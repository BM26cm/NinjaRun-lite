#ifndef START_H
#define START_H
#include "NinjaNameMaker.h"
#include "dojomap.h"
#include <iostream>
#include <string>
#include <sstream>
#include "ninja.h"
#include <list>
using namespace std;
class Start
{
    bool clansymbolDestroyed;
    Ninja yourninja;
    DojoMap dm;
    list<string> diary;
    vector<string> maps;
    bool loop;
    bool ninjaDead;
public:
    Start():clansymbolDestroyed(false), loop(false), ninjaDead(false){}
    ~Start(){}
    void startProgram(){

        makeName();

        mapChoice();

        yourninja.setCoordinata(dm.getStatPoint());

        int b=0;
        while(clansymbolDestroyed==false && !loop && !ninjaDead ){
            nextTurn();
            if(b>20){//hanyadik kortol keresen ismetlodest
                checkLoop();
            }
            b++;
        }
        if(!loop){//kiirja a ninja lepeseit vagy hogy loop
            for(auto it:diary){
                cout<<it<<endl;
            }
        }else{
            cout<<"LOOP"<<endl;
        }
    }
    void makeName(){
        string name;
        NinjaNameMaker nmk;
        string ninja_name;
        bool nameCorrect=false;
        while(!nameCorrect){
            cout << "give your name: " << endl;
            getline(cin, name);
            try{
                ninja_name=nmk.makeName(name);
                nameCorrect=true;
                cout<<" Your ninjaname: "<<ninja_name<<endl;
            }catch(string e){
                cout<<e<<endl;
            }
        }
    }
    void mapChoice(){
        cout<<"Choice map: "<<endl;

        ifstream input("input-all.list");
        if(input.is_open()){
            string line;
            while(getline(input, line)){
                maps.push_back(line);
            }
        }
        int mapnumber;
        bool correctMap=false;
        while(!correctMap){
            try{
                cout<<"Give map number (1 to "<<maps.size()<<"):"<<endl;
                cin>>mapnumber;
                if( !(cin.fail()) && mapnumber>0 && mapnumber<maps.size()){
                    correctMap=true;
                    dm.mapInput(maps[mapnumber-1]);
                    dm.drawMap();
                }else{
                    cin.clear();
                    cin.ignore(999,'\n');
                    throw string("Wrong map number");
                }
            }catch(string e){
                cout<<e<<endl;
            }
        }
    }
    void checkLoop(){//teljesíthető-e naplo fajl fojamatos ellenorzese (b-vel megadott kor utan kezdve) ismetlodo karakterlancot keresve ha az 3 szor ismetlodik loop van benne
        //naplo atirasa karakterlanca map segitsegevel adott lepest egy karakter jelol igy alkotva meg a naplo string valtozatat, elejeteol csokkeno seged stringet keresem hanyszor van meg benne
        char a='a';
        stringstream ss;
        map<string, char> segedmap;
        string naploString;
        segedmap.insert(pair<string,char>(diary.front(),a));
        a++;
        for (list<string>::iterator it = diary.begin(); it != diary.end(); it++){//map letrehozasa a lepesek char ba valo kodolasahoz
            map<string, char>::iterator mit =segedmap.find(*it);
            string asd;
            if(mit!=segedmap.end()){

                asd=mit->first;
            }
            if(asd.empty()){
                segedmap.insert(pair<string, char>(*it,a));
                a++;
            }
        }
        for(list<string>::iterator it = diary.begin(); it != diary.end(); it++){//map-bol felepiteni a naplo strin valtozatat
            auto mit =segedmap.find(*it);
            ss<<mit->second;
        }
        naploString=ss.str();
        //loopcheck -> ismetlodo karakterlanc keresese, kezdetben a naplostringel aztan annak egyre kissebb resz stringjevel (elejebol vesszuk le a karaktereket mert a vegen lesz az ismetlodes amit keresunk)

        int segedinNaplo=0;//karakterlanc hanyszor ismetlodik
        string segedString=naploString; //segedstringet keressük a naplo stringbe 1 szer mindenképp benne lesz harommal mar biztosabbak lehetunk a loopban
        while(segedString.size()>3 && !loop){//minimalis resz string megadasa valoszinuleg 2 parancsnal hosszabb loop ok jönnek letre

            for (size_t offset = naploString.find(segedString); offset != std::string::npos;//ismetlodes keresese, megtalalasa, megszamlalasa
                 offset = naploString.find(segedString, offset + segedString.length()))
            {
                ++segedinNaplo;
            }
            if(segedinNaplo>2){//hanyszor ismetlodik a karakterlanc
                loop=true;

            }
            string newseged;//a vizsgalt karakterlanc csökkentese
            int j=1;
            for(auto it=segedString.begin(); it!=segedString.end();it++){
                if(j>1){
                    newseged+=*it;
                }
                j++;
            }
            segedString=newseged;
            segedinNaplo=0;
        }
    }
    void nextTurn(){//egy kor
        clansymbolCheck();
        if(!clansymbolDestroyed ){
            if(yourninja.canthrowShuriken()){
                throwShuriken();
            }
            moveNinja();
            bombCheck();
        }
    }
    void destroySymbolwithShuriken(){
        diary.push_back("THROW (throws a shuriken to destroy the Holy Symbol)");
        clansymbolDestroyed=true;
        diary.push_back("GAME OVER - Destroy the Holy Symbol with the Shuriken.");
    }
    void destroySymbolwithBreak(){
        diary.push_back("BREAK - clansymbol");
        clansymbolDestroyed=true;
        diary.push_back("GAME OVER - Destroy the Holy Symbol with Break.");
    }

    void clansymbolCheck(){//elpusztítható e a clán szimbolum ha igen akkor elpusztitasa
        if(yourninja.getBreakmod()==true){//breakmodban-kézzel

            if(dm.getMapPoints()[yourninja.getCoordinata()+dm.getXlong()]->getObject()=='$'){
                destroySymbolwithBreak();
            }else if(dm.getMapPoints()[yourninja.getCoordinata()+1]->getObject()=='$'){
                destroySymbolwithBreak();
            }else if(dm.getMapPoints()[yourninja.getCoordinata()-1]->getObject()=='$'){
                 destroySymbolwithBreak();
            }else if(dm.getMapPoints()[yourninja.getCoordinata()-dm.getXlong()]->getObject()=='$'){
                 destroySymbolwithBreak();
            }
        }else if(yourninja.canthrowShuriken()){//shurikennel
            char direction='s';
            for(int j=yourninja.getCoordinata(); j<dm.getMapPoints().size() && clansymbolDestroyed==false && direction=='s'; j+=dm.getXlong()){
                    if(dm.getMapPoints()[j]->getObject()=='$'){
                        yourninja.attackShuriken(dm.getMapPoints()[j]);
                        destroySymbolwithShuriken();
                    }else if(dm.getMapPoints()[j]->getObject()=='#' || dm.getMapPoints()[j]->getObject()=='M' || dm.getMapPoints()[j]->getObject()=='X'){
                        direction='e';
                    }
            }
            for(int j=yourninja.getCoordinata(); j<dm.getMapPoints().size() && clansymbolDestroyed==false && direction=='e'; j++){
                     if(dm.getMapPoints()[j]->getObject()=='$'){
                        yourninja.attackShuriken(dm.getMapPoints()[j]);
                        destroySymbolwithShuriken();
                    }else if(dm.getMapPoints()[j]->getObject()=='#' || dm.getMapPoints()[j]->getObject()=='M' || dm.getMapPoints()[j]->getObject()=='X'){
                        direction='n';
                    }

            }
            for(int j=yourninja.getCoordinata();
                 j>=yourninja.getCoordinata()%dm.getXlong() && clansymbolDestroyed==false && direction=='n'; j-=dm.getXlong()){
                    if(dm.getMapPoints()[j]->getObject()=='$'){
                        yourninja.attackShuriken(dm.getMapPoints()[j]);
                        destroySymbolwithShuriken();
                    }else if(dm.getMapPoints()[j]->getObject()=='#' || dm.getMapPoints()[j]->getObject()=='M' || dm.getMapPoints()[j]->getObject()=='X'){
                        direction='w';
                    }
            }
            for(int j=yourninja.getCoordinata();
               j>=yourninja.getCoordinata()-(yourninja.getCoordinata()%dm.getXlong()) && clansymbolDestroyed==false && direction=='w'; j--){
                    if(dm.getMapPoints()[j]->getObject()=='$'){
                        yourninja.attackShuriken(dm.getMapPoints()[j]);
                        destroySymbolwithShuriken();
                    }else if(dm.getMapPoints()[j]->getObject()=='#' || dm.getMapPoints()[j]->getObject()=='M' || dm.getMapPoints()[j]->getObject()=='X'){
                        direction='s';
                    }
            }

        }
    }
    void throwShuriken(){//shuriken dobás elpusztithato objectumra
            char direction='s';
            for(int j=yourninja.getCoordinata(); j<dm.getMapPoints().size() && clansymbolDestroyed==false && direction=='s' && yourninja.canthrowShuriken(); j+=dm.getXlong()){
                    if(dm.getMapPoints()[j]->getObject()=='X'){
                        yourninja.attackShuriken(dm.getMapPoints()[j]);
                        diary.push_back("THROW (throws a shuriken to destroy X)");
                    }else if(dm.getMapPoints()[j]->getObject()=='#' || dm.getMapPoints()[j]->getObject()=='M'){
                        direction='e';
                    }
            }
            for(int j=yourninja.getCoordinata(); j<dm.getMapPoints().size() && clansymbolDestroyed==false && direction=='e'&& yourninja.canthrowShuriken(); j++){
                     if(dm.getMapPoints()[j]->getObject()=='X'){
                        yourninja.attackShuriken(dm.getMapPoints()[j]);
                        diary.push_back("THROW (throws a shuriken to destroy X)");
                    }else if(dm.getMapPoints()[j]->getObject()=='#' || dm.getMapPoints()[j]->getObject()=='M'){
                        direction='n';
                    }

            }
            for(int j=yourninja.getCoordinata();
                 j>=yourninja.getCoordinata()%dm.getXlong() && clansymbolDestroyed==false && direction=='n' && yourninja.canthrowShuriken(); j-=dm.getXlong()){
                    if(dm.getMapPoints()[j]->getObject()=='X'){
                        yourninja.attackShuriken(dm.getMapPoints()[j]);
                        diary.push_back("THROW (throws a shuriken to destroy X)");
                    }else if(dm.getMapPoints()[j]->getObject()=='#' || dm.getMapPoints()[j]->getObject()=='M'){
                        direction='w';
                    }
            }
            for(int j=yourninja.getCoordinata();
               j>=yourninja.getCoordinata()-(yourninja.getCoordinata()%dm.getXlong()) && clansymbolDestroyed==false && direction=='w' && yourninja.canthrowShuriken(); j--){
                    if(dm.getMapPoints()[j]->getObject()=='X'){
                        yourninja.attackShuriken(dm.getMapPoints()[j]);
                        diary.push_back("THROW (throws a shuriken to destroy X)");
                    }else if(dm.getMapPoints()[j]->getObject()=='#' || dm.getMapPoints()[j]->getObject()=='M'){
                        direction='s';
                    }
            }

    }
    void mirror(){  //ninja prioritas tukrozese
        if(yourninja.getMirrored()){
            yourninja.setMirrored(false);
        }else{
            yourninja.setMirrored(true);
        }
    }
    void moveNinja(){//ninja mozgasa a terkepen
        stringstream ss1,ss2;
        char a;
        bool move=false;


        a=dm.getMapPoints()[yourninja.getCoordinata()]->getObject();
        bool pair=false;
        switch (a) {
        case 'B':
            if(yourninja.getBreakmod()){
                yourninja.setBreakmod(false);
                //ss1<<", breakmod off)";

                ss1<<" (current direction)";
            }else{
                yourninja.setBreakmod(true);
                //ss1<<", breakmod on)";
                ss1<<" (current direction)";
            }
            break;
        case '*':
            yourninja.setShuriken(yourninja.getShuriken()+1);
            dm.getMapPoints()[yourninja.getCoordinata()]->setObject(' ');
            ss1<<" (after picked up the shuriken)";
            break;
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
            for(int j=0; j<dm.getMapPoints().size() && !pair ; j++){
                if(dm.getMapPoints()[j]->getObject()==a && j!=yourninja.getCoordinata()){
                    yourninja.setCoordinata(j);
                    pair=true;
                }
            }
            ss1<<" (current direction, after using the secret pathway "<<dm.getMapPoints()[yourninja.getCoordinata()]->getObject()<<")";
            break;
        case 'S':
            yourninja.setDirection(a);
            ss1<<" (change of direction caused by "<<dm.getMapPoints()[yourninja.getCoordinata()]->getObject()<<" )";
            break;
        case 'E':
            yourninja.setDirection(a);
            ss1<<" (change of direction caused by "<<dm.getMapPoints()[yourninja.getCoordinata()]->getObject()<<" )";
            break;
        case 'N':
            yourninja.setDirection(a);
            ss1<<" (change of direction caused by "<<dm.getMapPoints()[yourninja.getCoordinata()]->getObject()<<" )";
            break;
        case 'W':
            yourninja.setDirection(a);
            ss1<<" (change of direction caused by "<<dm.getMapPoints()[yourninja.getCoordinata()]->getObject()<<" )";
            break;
        default:
            ss1<<" (current direction)";
            break;
        }
        char pathblock;
        switch (yourninja.getDirection()) {//ninja mozog a 'direction'-je iranyaba ha tud
        case 'S':
            a=dm.getMapPoints()[yourninja.getCoordinata()+dm.getXlong()]->getObject();
            if( a == 'M'){
                mirror();
                pathblock=a;
            }else if( a=='X'){
                if(yourninja.getBreakmod()){
                    yourninja.setCoordinata(yourninja.getCoordinata()+dm.getXlong());
                    //naplo.push_back("BREAK (Break to destroy X)");
                    move=true;
                    dm.getMapPoints()[yourninja.getCoordinata()]->setObject(' ');
                }else{
                    pathblock=a;
                }
            }else if(a == '#' || a== '$'){
                    pathblock=a;
            }else{
                move=true;
                ss2<<"SOUTH";
                yourninja.setCoordinata(yourninja.getCoordinata()+dm.getXlong());
            }
            break;
        case 'E':
            a=dm.getMapPoints()[yourninja.getCoordinata()+1]->getObject();
            if( a == 'M'){
                mirror();
                pathblock=a;
            }else if( a=='X'){
                if(yourninja.getBreakmod()){
                    //naplo.push_back("BREAK (Break to destroy X)");
                    move=true;
                    yourninja.setCoordinata(yourninja.getCoordinata()+1);
                    dm.getMapPoints()[yourninja.getCoordinata()]->setObject(' ');
                }else{

                    pathblock=a;
                }
            }else if(a == '#' || a== '$'){
                pathblock=a;
            }else{
                move=true;
                ss2<<"EAST";
                yourninja.setCoordinata(yourninja.getCoordinata()+1);
            }
            break;
        case 'N':
            a=dm.getMapPoints()[yourninja.getCoordinata()-dm.getXlong()]->getObject();
            if( a == 'M'){
                mirror();
                pathblock=a;
            }else if( a=='X'){
                if(yourninja.getBreakmod()){
                    //naplo.push_back("BREAK (Break to destroy X)");
                    move=true;
                    yourninja.setCoordinata(yourninja.getCoordinata()-dm.getXlong());
                    dm.getMapPoints()[yourninja.getCoordinata()]->setObject(' ');
                }else{

                    pathblock=a;
                }
            }else if(a == '#' || a== '$'){
                pathblock=a;
            }else{
                move=true;
                ss2<<"NORTH";
                yourninja.setCoordinata(yourninja.getCoordinata()-dm.getXlong());
            }
            break;
        case 'W':
            a=dm.getMapPoints()[yourninja.getCoordinata()-1]->getObject();
            if( a == 'M'){
                mirror();
                pathblock=a;
            }else if( a=='X'){
                if(yourninja.getBreakmod()){
                    //naplo.push_back("BREAK (Break to destroy X)");
                    move=true;
                    yourninja.setCoordinata(yourninja.getCoordinata()-1);
                    dm.getMapPoints()[yourninja.getCoordinata()]->setObject(' ');
                }else{

                    pathblock=a;
                }
            }else if(a == '#' || a== '$'){
                pathblock=a;
            }else{
                move=true;
                ss2<<"WEST";
                yourninja.setCoordinata(yourninja.getCoordinata()-1);
            }
            break;
        default:
            break;
        }
//mikkor nem tud lepni a 'direction'-e iranyaba
        if(!move){
            if(!yourninja.getMirrored()){
                a=dm.getMapPoints()[yourninja.getCoordinata()+dm.getXlong()]->getObject();
                if(a != '#' && a!= '$' && a!='X' && a!='M'){
                    yourninja.setDirection('S');
                    ss2<<"SOUTH (because of the obstacle "<<pathblock<<" )";
                    yourninja.setCoordinata(yourninja.getCoordinata()+dm.getXlong());
                }else{
                    a=dm.getMapPoints()[yourninja.getCoordinata()+1]->getObject();
                    if(a != '#' && a!= '$' && a!='X'&& a!='M'){
                        yourninja.setDirection('E');
                         ss2<<"EAST (because of the obstacle "<<pathblock<<" )";
                        yourninja.setCoordinata(yourninja.getCoordinata()+1);
                    }else{
                        a=dm.getMapPoints()[yourninja.getCoordinata()-dm.getXlong()]->getObject();
                        if(a != '#' && a!= '$' && a!='X'&&  a!='M'){
                            yourninja.setDirection('N');
                            ss2<<"NORTH (because of the obstacle "<<pathblock<<" )";
                            yourninja.setCoordinata(yourninja.getCoordinata()-dm.getXlong());
                        }else{
                            a=dm.getMapPoints()[yourninja.getCoordinata()-1]->getObject();
                            if(a != '#' && a!= '$' && a!='X' &&  a!='M'){
                                yourninja.setDirection('W');
                                ss2<<"WEST (because of the obstacle "<<pathblock<<" )";
                                yourninja.setCoordinata(yourninja.getCoordinata()-1);
                            }
                        }
                    }
                }
            }else{
                a=dm.getMapPoints()[yourninja.getCoordinata()-1]->getObject();
                if(a != '#' && a!= '$' && a!='X' &&  a!='M'){
                    yourninja.setCoordinata(yourninja.getCoordinata()-1);
                    yourninja.setDirection('W');
                    ss2<<"WEST (because of the obstacle "<<pathblock<<" )";
                }else{
                    a=dm.getMapPoints()[yourninja.getCoordinata()-dm.getXlong()]->getObject();
                    if(a != '#' && a!= '$' && a!='X' &&  a!='M'){
                        yourninja.setCoordinata(yourninja.getCoordinata()-dm.getXlong());
                        yourninja.setDirection('N');
                        ss2<<"NORTH (because of the obstacle "<<pathblock<<" )";
                    }else{
                        a=dm.getMapPoints()[yourninja.getCoordinata()+1]->getObject();
                        if(a != '#' && a!= '$' && a!='X' &&  a!='M'){
                            yourninja.setCoordinata(yourninja.getCoordinata()+1);
                            yourninja.setDirection('E');
                            ss2<<"EAST (because of the obstacle "<<pathblock<<" )";
                        }else{
                            a=dm.getMapPoints()[yourninja.getCoordinata()+dm.getXlong()]->getObject();
                            if(a != '#' && a!= '$' && a!='X' &&  a!='M'){
                                yourninja.setCoordinata(yourninja.getCoordinata()+dm.getXlong());
                                yourninja.setDirection('S');
                                ss2<<"SOUTH (because of the obstacle "<<pathblock<<" )";
                            }
                        }
                    }
                }
            }
        }
        string seged=ss1.str();
        if(ss2.str().length()<7){

            ss2<<seged;
        }
        diary.push_back(ss2.str());

    }

    void bombCheck(){
        for(auto it:dm.getBombs()){
            if(it->getCoordinata()+1==yourninja.getCoordinata() || it->getCoordinata()+2==yourninja.getCoordinata() || it->getCoordinata()-1==yourninja.getCoordinata() || it->getCoordinata()-2==yourninja.getCoordinata() ||
                    it->getCoordinata()+dm.getXlong()==yourninja.getCoordinata() || it->getCoordinata()+(2*dm.getXlong())==yourninja.getCoordinata() || it->getCoordinata()-dm.getXlong()==yourninja.getCoordinata() || it->getCoordinata()-(2*dm.getXlong())==yourninja.getCoordinata())

            {
                it->setActive(true);
            }
        }
        for(auto it:dm.getBombs()){//vegig megy a bombakon
            if(it->getActive()){//megnezi mejik aktiv
                if(dm.getMapPoints()[it->getCoordinata()]->getObject()>'1'){//ha tobb mint 1 akkor csokkenti az erteket

                    dm.getMapPoints()[it->getCoordinata()]->setObject(((dm.getMapPoints()[it->getCoordinata()]->getObject()-'0')-1)+'0');

                }else if(dm.getMapPoints()[it->getCoordinata()]->getObject()=='1'){//ha egy akkor robban

                    bombDetonate(it);
                    it->setDetonated(true);

                    dm.getMapPoints()[it->getCoordinata()]->setObject('0');

                }
            }
        }
    }
    void bombDetonate(Bomb* it){
                    diary.push_back("Bomb Detonate");
                    //ninja bomba kozeleben van-e
                    if(it->getCoordinata()+1==yourninja.getCoordinata() || it->getCoordinata()+2==yourninja.getCoordinata() || it->getCoordinata()-1==yourninja.getCoordinata() || it->getCoordinata()-2==yourninja.getCoordinata() ||
                            it->getCoordinata()+dm.getXlong()==yourninja.getCoordinata() || it->getCoordinata()+(2*dm.getXlong())==yourninja.getCoordinata() || it->getCoordinata()-dm.getXlong()==yourninja.getCoordinata() || it->getCoordinata()-(2*dm.getXlong())==yourninja.getCoordinata())
                    {

                        ninjaDead=true;
                        diary.push_back("GAME OVER (ninja dead by bomb)");

                    }
                    //'X'-ek pusztulasa
                    char a=dm.getMapPoints()[it->getCoordinata()+dm.getXlong()]->getObject();
                    if(a!='$' && a!='#' && a!='M'){
                        if(a=='X' || a=='*'){
                            dm.getMapPoints()[it->getCoordinata()+dm.getXlong()]->setObject(' ');
                        }
                        a=dm.getMapPoints()[it->getCoordinata()+(2*dm.getXlong())]->getObject();
                        if(a=='X' || a=='*'){
                            dm.getMapPoints()[it->getCoordinata()+(2*dm.getXlong())]->setObject(' ');
                        }
                    }
                    a=dm.getMapPoints()[it->getCoordinata()+1]->getObject();
                    if(a!='$' && a!='#' && a!='M'){
                         if(a=='X' || a=='*'){
                        dm.getMapPoints()[it->getCoordinata()+1]->setObject(' ');
                         }
                        a=dm.getMapPoints()[it->getCoordinata()+2]->getObject();
                        if(a=='X' || a=='*'){
                            dm.getMapPoints()[it->getCoordinata()+2]->setObject(' ');
                        }
                    }
                    a=dm.getMapPoints()[it->getCoordinata()-1]->getObject();
                    if( a!='$' && a!='#' && a!='M'){
                         if(a=='X' || a=='*'){
                        dm.getMapPoints()[it->getCoordinata()-1]->setObject(' ');
                         }
                        a=dm.getMapPoints()[it->getCoordinata()-2]->getObject();
                        if(a=='X' || a=='*'){
                            dm.getMapPoints()[it->getCoordinata()-2]->setObject(' ');
                        }
                    }
                    a=dm.getMapPoints()[it->getCoordinata()-dm.getXlong()]->getObject();
                    if(a!='$' && a!='#' && a!='M'){
                        if(a=='X' || a=='*'){
                        dm.getMapPoints()[it->getCoordinata()-dm.getXlong()]->setObject(' ');
                        }

                        a=dm.getMapPoints()[it->getCoordinata()-(2*dm.getXlong())]->getObject();
                        if(a=='X'|| a=='*'){
                            dm.getMapPoints()[it->getCoordinata()-(2*dm.getXlong())]->setObject(' ');
                        }
                    }
                    //tovabbi bombak robbannak-e
                    for(auto i:dm.getBombs()){
                        if(!i->getDetonated()){
                            //bombatol keletnek
                            if(i->getCoordinata()==it->getCoordinata()+1){
                                dm.getMapPoints()[i->getCoordinata()]->setObject('0');
                                i->setDetonated(true);
                                bombDetonate(i);

                            }//kelet +1
                            a=dm.getMapPoints()[it->getCoordinata()+1]->getObject();
                            if(i->getCoordinata()==it->getCoordinata()+2 && a!='#' && a!='M' && a!='$'){
                                dm.getMapPoints()[i->getCoordinata()]->setObject('0');
                                i->setDetonated(true);
                                bombDetonate(i);
                            }//bombatol nyugatra
                            if(i->getCoordinata()==it->getCoordinata()-1){
                                dm.getMapPoints()[i->getCoordinata()]->setObject('0');
                                i->setDetonated(true);
                                bombDetonate(i);

                            }//nyugat+1
                            a=dm.getMapPoints()[it->getCoordinata()-1]->getObject();
                            if(i->getCoordinata()==it->getCoordinata()-2 && a!='#' && a!='M' && a!='$'){
                                dm.getMapPoints()[i->getCoordinata()]->setObject('0');
                                i->setDetonated(true);
                                bombDetonate(i);
                            }//bombatol delre
                            if(i->getCoordinata()==it->getCoordinata()+dm.getXlong()){
                                dm.getMapPoints()[i->getCoordinata()]->setObject('0');
                                i->setDetonated(true);
                                bombDetonate(i);

                            }
                            a=dm.getMapPoints()[it->getCoordinata()+dm.getXlong()]->getObject();
                            if(i->getCoordinata()==it->getCoordinata()+(2*dm.getXlong()) && a!='#' && a!='M' && a!='$'){
                                dm.getMapPoints()[i->getCoordinata()]->setObject('0');
                                i->setDetonated(true);
                                bombDetonate(i);
                            }//bombatol eszakra
                            if(i->getCoordinata()==it->getCoordinata()-dm.getXlong()){
                                dm.getMapPoints()[i->getCoordinata()]->setObject('0');
                                i->setDetonated(true);
                                bombDetonate(i);

                            }
                            a=dm.getMapPoints()[it->getCoordinata()-dm.getXlong()]->getObject();
                            if(i->getCoordinata()==it->getCoordinata()-(2*dm.getXlong()) && a!='#' && a!='M' && a!='$'){
                                dm.getMapPoints()[i->getCoordinata()]->setObject('0');
                                i->setDetonated(true);
                                bombDetonate(i);
                            }
                        }
                    }

    }

};

#endif // START_H
