#include "mapcoordinate.h"

mapCoordinate::mapCoordinate(int x,int y, char object):x(x),y(y),object(object)
{

}
int mapCoordinate::getX() const
{
    return x;
}

void mapCoordinate::setX(int value)
{
    x = value;
}

int mapCoordinate::getY() const
{
    return y;
}

void mapCoordinate::setY(int value)
{
    y = value;
}

char mapCoordinate::getObject() const
{
    return object;
}

void mapCoordinate::setObject(char value)
{
    object = value;
}

mapCoordinate::~mapCoordinate(){

}
