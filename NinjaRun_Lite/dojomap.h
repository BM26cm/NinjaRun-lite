#ifndef DOJOMAP_H
#define DOJOMAP_H
#include <string>
#include "mapcoordinate.h"
#include <fstream>
#include <vector>
#include <sstream>
#include "bomb.h"

using namespace std;
class DojoMap
{
private:
    vector<mapCoordinate*> mapPoints;
    int xlong, ylong;
    vector<Bomb*> bombs;
public:
    DojoMap(){}
    ~DojoMap(){
        for (vector<mapCoordinate*>::iterator it=mapPoints.begin(); it!=mapPoints.end(); ++it){
            delete *it;
        }
        for(vector<Bomb*>::iterator it=bombs.begin(); it!=bombs.end(); ++it){
            delete *it;
        }
    }
    void mapInput(string map_filename){
        ifstream input(map_filename);
        int b=0;
        if(input.is_open()){
            string line;
            int y=0;
            while(getline(input, line)){
                if(line.length()>0){xlong=line.length();}
                int x=0;
                y++;
                for(char& c:line){
                    x++;

                    mapCoordinate *newcordinat=new mapCoordinate(x,y,c);
                    mapPoints.push_back(newcordinat);
                    //Bomba
                    if(isdigit(c)){
                        Bomb *newBomb=new Bomb(b);
                        bombs.push_back(newBomb);
                    }
                    b++;
                    //
                }
            }
            ylong=y;
        }
    }
    void drawMap(){
        for(mapCoordinate* it : mapPoints){
            cout<<it->getObject();
            if(it->getX()==xlong){cout<<endl;}
        }
    }
    int getStatPoint() const
    {
        for(int j=0; j<mapPoints.size(); j++){
            if(mapPoints[j]->getObject()=='@'){
                return j;
            }
        }
    }
    vector<mapCoordinate *> getMapPoints() const{
        return mapPoints;
    }
    void setMapPoints(const vector<mapCoordinate *> &value){
        mapPoints = value;
    }
    int getYlong() const{
        return ylong;
    }
    void setYlong(int value){
        ylong = value;
    }
    int getXlong() const{
        return xlong;
    }
    void setXlong(int value){
        xlong = value;
    }
    vector<Bomb *> getBombs() const{
        return bombs;
    }
    void setBombs(const vector<Bomb *> &value){
        bombs = value;
    }
};

#endif // DOJOMAP_H

