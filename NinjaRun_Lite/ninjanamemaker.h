#ifndef NINJANAMEMAKER_H
#define NINJANAMEMAKER_H
#include <iostream>
#include <sstream>
#include <map>
#include <string>

using namespace std;

class NinjaNameMaker{
private:
    map<char, string> ninja_abc;
public:
    NinjaNameMaker(){
        ninja_abc.insert(pair<char, string>('a', "ka"));
        ninja_abc.insert(pair<char, string>('b', "zu"));
        ninja_abc.insert(pair<char, string>('c', "mi"));
        ninja_abc.insert(pair<char, string>('d', "te"));
        ninja_abc.insert(pair<char, string>('e', "ku"));
        ninja_abc.insert(pair<char, string>('f', "lu"));
        ninja_abc.insert(pair<char, string>('g', "ji"));
        ninja_abc.insert(pair<char, string>('h', "ri"));
        ninja_abc.insert(pair<char, string>('i', "ki"));
        ninja_abc.insert(pair<char, string>('j', "zu"));
        ninja_abc.insert(pair<char, string>('k', "me"));
        ninja_abc.insert(pair<char, string>('l', "ta"));
        ninja_abc.insert(pair<char, string>('m', "rin"));
        ninja_abc.insert(pair<char, string>('n', "to"));
        ninja_abc.insert(pair<char, string>('o', "mo"));
        ninja_abc.insert(pair<char, string>('p', "no"));
        ninja_abc.insert(pair<char, string>('q', "ke"));
        ninja_abc.insert(pair<char, string>('r', "shi"));
        ninja_abc.insert(pair<char, string>('s', "ari"));
        ninja_abc.insert(pair<char, string>('t', "chi"));
        ninja_abc.insert(pair<char, string>('u', "do"));
        ninja_abc.insert(pair<char, string>('v', "ru"));
        ninja_abc.insert(pair<char, string>('w', "mei"));
        ninja_abc.insert(pair<char, string>('x', "na"));
        ninja_abc.insert(pair<char, string>('y', "fu"));
        ninja_abc.insert(pair<char, string>('z', "zi"));
    }
    ~NinjaNameMaker(){}
    string makeName(const string& name)const{
        string ninja_name;
        stringstream ss;
        for(char c : name){
            if(c!=' '){
                if((c>='A' && c<='Z') || (c>='a' && c<='z')){
                    ss<<ninja_abc.find(tolower(c))->second;
                }else{
                    throw string("Wrong name");
                }
            }else if(c==' ') {
                ss<<" ";
            }
        }
        ninja_name=ss.str();
        int j=0;
        for(char it: ninja_name){
            if(it==' ' ){
                j++;
                ninja_name[j]=toupper(ninja_name[j]);
            }else{
                j++;
            }
        }
        ninja_name[0]=toupper(ninja_name[0]);
        return ninja_name;
    }


};
#endif // NINJANAMEMAKER_H
