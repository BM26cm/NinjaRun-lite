#ifndef MAPCOORDINATE_H
#define MAPCOORDINATE_H


class mapCoordinate
{
private:
    int x,y;
    char object;
public:
    mapCoordinate(int x, int y, char object);
    ~mapCoordinate();


    int getX() const;
    void setX(int value);
    int getY() const;
    void setY(int value);
    char getObject() const;
    void setObject(char value);
};

#endif // MAPCOORDINATE_H
