#ifndef BOMB_H
#define BOMB_H

class Bomb{
private:
    int coordinata;
    bool active;
    bool detonated;
public:
    Bomb(int coordinata):coordinata(coordinata),active(false), detonated(false){}
    ~Bomb(){}

    int getCoordinata() const{
        return coordinata;
    }
    void setCoordinata(int value){
        coordinata = value;
    }
    bool getActive() const{
        return active;
    }
    void setActive(bool value){
        active = value;
    }

    bool getDetonated() const{
        return detonated;
    }
    void setDetonated(bool value){
        detonated = value;
    }

};

#endif // BOMB_H
