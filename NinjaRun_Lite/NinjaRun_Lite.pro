TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG +=c++11

SOURCES += \
        main.cpp \
    mapcoordinate.cpp \
    ninjanamemaker.cpp

HEADERS += \
    ninjanamemaker.h \
    dojomap.h \
    mapcoordinate.h \
    ninja.h \
    start.h \
    bomb.h

#QMAKE_CXXFLAGS += -std=c++0x -pthread
#LIBS += -pthread
