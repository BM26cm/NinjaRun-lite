#ifndef NINJA_H
#define NINJA_H
#include "dojomap.h"

using namespace std;
class Ninja{
private:
    int shuriken;
    bool breakmod;
    bool mirrored;
    int coordinata;
    char direction;
public:


    Ninja():breakmod(false),shuriken(3),direction('S'),mirrored(false){}
    ~Ninja(){}

    bool canthrowShuriken()const{
       if(shuriken>0){//amig van suriken
            return true;
        }
       return false;
    }

    void attackShuriken(mapCoordinate* mC){
        shuriken--;
        mC->setObject('*');

    }
    void turn(char newd){
        direction=newd;
    }


    bool getBreakmod() const
    {
        return breakmod;
    }

    void setBreakmod(bool newvalue)
    {
        breakmod = newvalue;
    }

    int getShuriken() const{
    return shuriken;
    }
    void setShuriken(int value){
    shuriken = value;
    }
    int getCoordinata() const{
    return coordinata;
    }
    void setCoordinata(int value){
    coordinata = value;
    }

    bool getMirrored() const;
    void setMirrored(bool value);
    char getDirection() const;
    void setDirection(char value);
};
#endif // NINJA_H

char Ninja::getDirection() const
{
    return direction;
}

void Ninja::setDirection(char value)
{
    direction = value;
}

bool Ninja::getMirrored() const
{
    return mirrored;
}

void Ninja::setMirrored(bool value)
{
    mirrored = value;
}
